# Vem ni mim #

Repositório para os projetos utilizados nos eventos Vem ni mim.

## Dojo Commons Collections ##
### Regras ###
* Sobre commons collections, só pode consultar no javadoc;
* Pode haver no máximo uma estrutura de loop em todo código, incluindo testes;
* O código deve ter boa qualidade e legibilidade (time decide);
* Não pode usar debug, mas pode ver o fonte;
* Asserts devem verificar os elementos, não só o tamanho da coleção;
* Os resultados devem ser impressos no console para visualização de todos se alguém solicitar;
* Turnos de 5 minutos, em dupla.

### Atividades ###
Devemos implementar as seguintes funcionalidades em um repositório de clientes:

* findAll
* findAllDeMinasGerais
* printSobrenomeMaiusculoVirgulaNome
* findAllMulheresEDeSaoPaulo
* findAllAdultosHomensOuMulheresEDoRio
* findAllComNomeIniciandoComBOuJDistinct
* updateAllCasadosParaMinasGerais
* findAllMulheresOuDeSaoPauloNaoCasadosDistinct
* sumIdadesDeHomesMenoresDe30AnosSolteiros
* findAllGroupByEstado