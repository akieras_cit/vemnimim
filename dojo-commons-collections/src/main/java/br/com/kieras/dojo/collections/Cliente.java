package br.com.kieras.dojo.collections;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
@SuppressWarnings("javadoc")
public class Cliente {

    @NonNull
    private Integer id;

    @NonNull
    private String nome;

    @NonNull
    private String sobrenome;

    @NonNull
    private Integer idade;

    @NonNull
    private Sexo sexo;

    @NonNull
    private Estado estado;

    private Cliente conjuge;

    public enum Sexo {
        M, F;
    }

    public enum Estado {
        MG, SP, RJ, ES;
    }

}
