package br.com.kieras.dojo.collections;

import java.util.List;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.PredicateUtils;

@RequiredArgsConstructor
@SuppressWarnings("javadoc")
public class ClienteRepository {

    @Getter
    @Setter
    @NonNull
    private List<Cliente> clientes;

    public List<Cliente> findAll() {
        return (List<Cliente>) CollectionUtils.select(clientes,
                PredicateUtils.truePredicate());
    }

}
