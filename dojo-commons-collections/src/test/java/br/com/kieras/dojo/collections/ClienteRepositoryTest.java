package br.com.kieras.dojo.collections;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import br.com.kieras.dojo.collections.Cliente.Estado;
import br.com.kieras.dojo.collections.Cliente.Sexo;

@SuppressWarnings("javadoc")
public class ClienteRepositoryTest {

    @Rule
    public TestRule watcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            System.out.println("\n\n===============" + description.getMethodName());
        }
    };

    private static ClienteRepository repository;

    @BeforeClass
    public static void setupRepository() throws Exception {
        List<Cliente> clientes =
            new LinkedList<Cliente>(
                    //@formatter:off
                    Arrays.asList(
                        new Cliente(1, "Bob", "Wolfram", 32, Sexo.M, Estado.MG), 
                        new Cliente(2, "Larry", "Paquin", 25, Sexo.M, Estado.MG), 
                        new Cliente(3, "Bill", "Nelson", 16, Sexo.M, Estado.MG), 
                        new Cliente(3, "Bill", "Nelson", 16, Sexo.M, Estado.MG), 
                        new Cliente(4, "Sue", "Wolfram", 27, Sexo.F, Estado.SP),
                        new Cliente(4, "Sue", "Wolfram", 27, Sexo.F, Estado.SP)
                    ));
                    //@formatter:on

        Cliente joe = new Cliente(5, "Joe", "Wolfram", 32, Sexo.M, Estado.SP);
        Cliente zoe = new Cliente(6, "Zoe", "Paquin", 28, Sexo.F, Estado.RJ);
        joe.setConjuge(zoe);
        zoe.setConjuge(joe);

        clientes.add(joe);
        clientes.add(zoe);

        repository = new ClienteRepository(clientes);
    }

    @Test
    public void testFindAll() {
        List<Cliente> clientes = repository.findAll();
        assertThat(clientes.size(), equalTo(8));
    }

}
